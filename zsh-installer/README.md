# zsh-installer

zsh installer with plugins

based on:

https://github.com/romkatv/zsh-bin

https://github.com/zplug/zplug

https://github.com/romkatv/powerlevel10k

install:

```bash
sh -c "$(curl -fsSL https://gitlab.com/azubi20201/pauls-utills/-/raw/master/zsh-installer/installzsh.sh)"
```

essentials: 

```bash
sh -c "$(curl -fsSL https://gitlab.com/azubi20201/pauls-utills/-/raw/master/zsh-installer/essentials.sh)"
```
